package pages.task1pages;

import base.BasePage;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SearchResultPage extends BasePage {

    @FindBy (xpath = "//ol[@class='b-results-list']//li[1]")
    List<WebElement>  searchList;

    public int  getSearchList(){
        return searchList.size();
    }

    public int getHTML() {
        String s = driver.getPageSource();
        List<String> resultsList= new ArrayList<String>();
        Matcher matcher = Pattern.compile("<li class=\"b-results__li\"").matcher(s);
        while (matcher.find()) {
            resultsList.add(matcher.group());
        }
        return resultsList.size();
    }
   }
