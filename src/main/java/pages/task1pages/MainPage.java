package pages.task1pages;

import base.BasePage;
import framework.PropertyProvider;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;


public class MainPage  extends BasePage {

    @FindBy (css = "input#search_from_str")
    private WebElement searchField;

    @FindBy (xpath = "//input[@name='search']")
    private WebElement searchBtn;

    String url = PropertyProvider.getProperty("start_page_task1");

    public MainPage typeKeyword(String keyword)
    {
        searchField.clear();
        searchField.sendKeys(keyword);
        return this;
    }

    public SearchResultPage clickSearchButton()
    {
        searchBtn.click();
        return new SearchResultPage();
    }

    public String getPageUrl() { return this.url; }

}
