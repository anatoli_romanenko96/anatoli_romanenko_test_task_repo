package pages.task3pages;

import base.BasePage;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ChoiceLanguagePage extends BasePage {

    @FindBy (xpath = "//input[@value='en']")
    WebElement languageRadioBtn;


    @FindBy (xpath = "//button[@id='langcontinue']")
    WebElement continueBtn;

    public  ChoiceLanguagePage clickLanguageRadioButton()
    {
        languageRadioBtn.click();
        return new ChoiceLanguagePage();
    }

    public  MainPageTask3 clickContinueButton()
    {
        continueBtn.click();
        return new MainPageTask3();
    }




}
