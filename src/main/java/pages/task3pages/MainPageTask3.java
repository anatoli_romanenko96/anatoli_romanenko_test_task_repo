package pages.task3pages;

import base.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.text.SimpleDateFormat;
import java.util.*;

public class MainPageTask3 extends BasePage {

    @FindBy(xpath = "//button[@class='toggle_form']")
    WebElement advancedSearchBtn;
    @FindBy(xpath = "//input[@id='hotelSearchOneBox']")
    WebElement placeField;
    @FindBy(xpath = "//a[@class='find_button cta_button findItButton']//span[@class='text'][contains(text(),'Go')]")
    WebElement goBtn;
    @FindBy(xpath = "//input[@id='checkin']")
    WebElement checkinField;
    @FindBy(xpath = "//input[@id='checkout']")
    WebElement checkoutField;

    public MainPageTask3 clickAdvancedSearchButton() {
        advancedSearchBtn.click();
        return new MainPageTask3();
    }

    public MainPageTask3 clickNumberOfAdultsSelect(String numberOfAdults) {
        By numberOfAdultSelector = By.xpath("//select[@id='room1Adults']//option[@value='" + numberOfAdults + "'][contains(text(),'" + numberOfAdults + "')]");
        driver.findElement(numberOfAdultSelector).click();
        return new MainPageTask3();
    }

    public MainPageTask3 clickNumberOfChildrenSelect(String childrenAmount) {
        By numberOfChildrenSelector = By.xpath("//select[@id='room1Children']//option[@value='" + childrenAmount + "'][contains(text(),'" + childrenAmount + "')]");
        driver.findElement(numberOfChildrenSelector).click();
        return new MainPageTask3();
    }

    public MainPageTask3 clickNumberOfRoomSelect(String numberOfRooms) {
        By numberOfRoomSelector = By.xpath("//select[@id='numberOfRooms']//option[@value='" + numberOfRooms + "'][contains(text(),'" + numberOfRooms + "')]");
        driver.findElement(numberOfRoomSelector).click();
        return new MainPageTask3();
    }

    public HotelListPage clickGoButton() {
        goBtn.click();
        return new HotelListPage();
    }

    public MainPageTask3 typePlace(String place) {
        placeField.clear();
        placeField.sendKeys(place);
        return this;
    }

    public MainPageTask3 setCheckInDate(String checkInDate) {
        checkinField.clear();
        checkinField.sendKeys(getDate(Integer.parseInt(checkInDate)));
        return new MainPageTask3();
    }

    public MainPageTask3 setCheckOutDate(String checkOutDate) {
        checkoutField.clear();
        checkoutField.sendKeys(getDate(Integer.parseInt(checkOutDate)));
        return new MainPageTask3();
    }

    private String getDate(int days) {
        GregorianCalendar calendar = new GregorianCalendar();
        SimpleDateFormat dateFormat = new SimpleDateFormat("d MMM yyyy", Locale.ENGLISH);
        calendar.add(Calendar.HOUR, days * 24);
        return dateFormat.format(calendar.getTime());
    }
}


