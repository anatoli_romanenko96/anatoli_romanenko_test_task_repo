package pages.task3pages;


import base.BasePage;
import org.apache.logging.log4j.LogManager;

import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class PriceDetailsPage extends BasePage {
    @FindBy (css = "span.total-dollars")
    WebElement totalForStay;

    private static final Logger log = LogManager.getLogger(PriceDetailsPage.class);

    public PriceDetailsPage getTotalForStay(){
        log.info("Total for stay: " + totalForStay.getText());
        return new PriceDetailsPage();
    }
}
