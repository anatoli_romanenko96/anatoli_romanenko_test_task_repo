package pages.task3pages;

import base.BasePage;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class HotelListPage extends BasePage {
    @FindBy (css = "div.btnBox")
    List <WebElement> selectHotelBtn;

    public  HotelPage clickSelectHotelButton()
    {
        selectHotelBtn.get(0).click();
        return new HotelPage();
    }

}
