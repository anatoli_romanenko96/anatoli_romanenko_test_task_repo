package pages.task3pages;

import base.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class PersonalInformationPage extends BasePage {

    @FindBy (xpath = "//input[@id='guestFirstName']")
    WebElement firstNameField;

    @FindBy (xpath = "//input[@id='guestLastName']")
    WebElement lastNameField;

    @FindBy (xpath = "//input[@id='guestPhone']")
    WebElement phoneField;
    @FindBy (xpath = "//input[@id='guestEmail']")
    WebElement emailField;
    @FindBy (xpath = "//input[@id='guestUSAddress1']")
    WebElement adressField;
    @FindBy (xpath = "//input[@id='guestUSZip']")
    WebElement zipField;
    @FindBy (xpath = " //input[@id='guestUSCity']")
    WebElement cityField;
    @FindBy (xpath = " //a[contains(@class,'linkBtn')]")
    WebElement continueToPriceBtn;


    public PersonalInformationPage typeFirstName(String firstName)
    {
        firstNameField.clear();
        firstNameField.sendKeys(firstName);
        return this;
    }
    public PersonalInformationPage typeLastName(String lastName)
    {
        lastNameField.clear();
        lastNameField.sendKeys(lastName);
        return this;
    }
    public PersonalInformationPage typePhoneNumber(String phoneNumber)
    {
        phoneField.clear();
        phoneField.sendKeys(phoneNumber);
        return this;
    }
    public PersonalInformationPage typeEmail(String email)
    {
        emailField.clear();
        emailField.sendKeys(email);
        return this;
    }
    public PersonalInformationPage typeZip(String zip)
    {
        zipField.clear();
        zipField.sendKeys(zip);
        return this;
    }
    public PersonalInformationPage typeAdress(String adress)
    {
        adressField.clear();
        adressField.sendKeys(adress);
        return this;
    }
    public PersonalInformationPage typeCity(String city)
    {
        cityField.clear();
        cityField.sendKeys(city);
        return this;
    }

    public PersonalInformationPage clickState(String state){
        By stateField= By.xpath("//option[contains(text(),'"+state+"')]");
        driver.findElement(stateField).click();
        return new PersonalInformationPage();
    }

    public PriceDetailsPage clickContinueToPriceDetails(){
        continueToPriceBtn.click();
        return new PriceDetailsPage();
    }
}
