package pages.task3pages;

import base.BasePage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.ArrayList;
import java.util.List;


public class HotelPage  extends BasePage {
    @FindBy (xpath = "//span[@class='priceamount currencyCode-USD']|//ins[@class='new-price']")
    List<WebElement> hotelPrices;

    @FindBy (xpath = "//a[@class='popup hasDigitalKey property-home-link']")
    WebElement hotelName;

    @FindBy (xpath = "//a[contains(text(),'More prices')]")
    List < WebElement> showMoreBtn;

    @FindBy (xpath = "//a[@class='linkBtn']")
    List<WebElement> allSelectButtons;

    private static final Logger log = LogManager.getLogger(PriceDetailsPage.class);

    public HotelPage clickAdditionalPricesToggle(){
        for (int i = 0; i <showMoreBtn.size() ; i++) {
            showMoreBtn.get(i).click();
        }
        return new HotelPage();
    }

    public PersonalInformationPage clickSelectPrice(){
        int a = 0;
        int number= 0;
        List <String> s= new ArrayList<>();
        for (int i = 0; i < hotelPrices.size() ; i++) {
            s.add(hotelPrices.get(i).getText());
        }
        List<Integer> pricesList = new ArrayList<>();
        for (int i = 0; i < s.size(); i++) {
            if (s.get(i).replaceAll("[^0-9]", "").equals("")){
                continue;
            }
                else{
                pricesList.add(Integer.parseInt(s.get(i).replaceAll("[^0-9]", "")));
            }
        }
        for (int i = 0; i < pricesList.size(); i++) {
            System.out.println(pricesList.get(i));
            if (pricesList.get(i)>a) {
                a=pricesList.get(i);
                number = i;
            }
        }
        log.info(hotelName.getText().substring(0,hotelName.getText().indexOf('\n')));
        allSelectButtons.get(number).click();
        return new PersonalInformationPage();
    }

}
