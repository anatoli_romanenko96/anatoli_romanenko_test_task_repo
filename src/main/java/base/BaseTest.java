package base;

import java.util.concurrent.TimeUnit;
import framework.BrowserFactory;
import framework.PropertyProvider;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

    public class BaseTest {

        protected static WebDriver driver = BrowserFactory.getDriver(PropertyProvider.getProperty("browser"));

        @BeforeTest
        public void before() {
            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        }

        @AfterTest
        public void tearDown() {
            if (driver != null) driver.quit();
        }
    }

