package base;

import framework.PropertyProvider;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class BasePage {
    protected WebDriver driver = BaseTest.driver;

    protected String url;

    public BasePage()
    {
        PageFactory.initElements(driver, this);
    }

    public String getURL(String s){
        return PropertyProvider.getProperty(s);
    }

    public void navigateToPageURL(String url){
        driver.get(url);
    }

}
