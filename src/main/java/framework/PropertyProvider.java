package framework;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Properties;

public class PropertyProvider {
    static Properties properties = new Properties();
    static String absolutePathToProperties = "src\\test\\resources\\commonProperties.properties";

    public static String getProperty(String propertyName) {
        File file = new File(absolutePathToProperties);
        try {
            properties.load(new FileInputStream(file));
            return new String(properties.getProperty(propertyName).getBytes(StandardCharsets.ISO_8859_1));
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}


