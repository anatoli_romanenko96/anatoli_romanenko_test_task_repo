package framework;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class BrowserFactory {
    public static WebDriver getDriver(String browserName)
    {
        switch (browserName) {
            case "CHROME": System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
                return new ChromeDriver();
            case "FIREFOX":
                System.setProperty("webdriver.gecko.driver", "geckodriver.exe");
                return new FirefoxDriver();
            default: System.out.print(browserName +"is not a valid parameter, switching to Chrome instead");
                System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
                return new ChromeDriver();
        }
    }
}