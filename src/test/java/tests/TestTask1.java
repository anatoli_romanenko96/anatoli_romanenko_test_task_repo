package tests;

import base.BaseTest;
import framework.PropertyProvider;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.task1pages.MainPage;
import pages.task1pages.SearchResultPage;
public class TestTask1 extends BaseTest {

    @Test
    public void testTask1()  {
        new MainPage().navigateToPageURL(new MainPage().getURL("start_page_task1"));
        new MainPage().typeKeyword(PropertyProvider.getProperty("keyword"))
                .clickSearchButton();
       Assert.assertEquals(new SearchResultPage().getSearchList(), new SearchResultPage().getHTML());
    }
}
