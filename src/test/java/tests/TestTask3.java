package tests;

import base.BaseTest;
import framework.PropertyProvider;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.task3pages.ChoiceLanguagePage;
import pages.task3pages.MainPageTask3;

import java.util.concurrent.TimeUnit;
//remove unused imports

public class TestTask3 extends BaseTest {

    @Test
    public void testTask3(){
            new ChoiceLanguagePage().navigateToPageURL(new ChoiceLanguagePage().getURL("start_page_task3"));
            new ChoiceLanguagePage().clickLanguageRadioButton()
                    .clickContinueButton()
                    .clickAdvancedSearchButton()
                    .clickNumberOfRoomSelect(PropertyProvider.getProperty("number_of_rooms"))
                    .clickNumberOfAdultsSelect(PropertyProvider.getProperty("number_of_adults"))
                    .clickNumberOfChildrenSelect(PropertyProvider.getProperty("number_of_children"))
                    .typePlace(PropertyProvider.getProperty("place"))
                    .setCheckInDate(PropertyProvider.getProperty("days_for_checkin"))
                    .setCheckOutDate(PropertyProvider.getProperty("days_for_checkout"))
                    .clickGoButton()
                    .clickSelectHotelButton()
                    .clickAdditionalPricesToggle()
                    .clickSelectPrice()
                    .typeFirstName(PropertyProvider.getProperty("guest_first_name"))
                    .typeLastName(PropertyProvider.getProperty("guest_last_name"))
                    .typePhoneNumber(PropertyProvider.getProperty("guest_phone_number"))
                    .typeEmail(PropertyProvider.getProperty("guest_email"))
                    .typeZip(PropertyProvider.getProperty("guest_zip"))
                    .typeCity(PropertyProvider.getProperty("guest_city"))
                    .typeAdress(PropertyProvider.getProperty("guest_adress"))
                    .clickState(PropertyProvider.getProperty("state"))
                    .clickContinueToPriceDetails()
                    .getTotalForStay();
        }
    }

